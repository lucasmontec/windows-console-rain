#define SCREEN_W 50
#define SCREEN_H 20
#define DROP_LENGTH 5

#define DROP_CHANCE 60
#define DROP_INTERVAL 500

#include <iostream>
using namespace std;

#include "arduino_time.h"

//This stores the next time the system will attempt to create a drop
unsigned long nextDropAttempt;

typedef struct drop {
  int X;
  float Y;
  float Speed;
  struct drop* next;
} drop;

drop* rootDrop;

void createDrop(int X, float Y, float Speed)
{
  //Create the drop
  drop* d = (drop*) malloc(sizeof(drop));
  d->X = X;
  d->Y = Y;
  d->Speed = Speed;
  d->next = NULL;
  
  //Add the drop to the drop list
  if(rootDrop == NULL)
  {
  	//cout << "CREATE SINGLE Try set first drop" << endl;
    //Add the first drop
    rootDrop = d;
    //cout << "CREATE SINGLE Set first drop" << endl;
    //cout << "CREATE SINGLE ROOT X " << rootDrop->X << " Y " << rootDrop->Y << " drop speed " << rootDrop->Speed << endl;
  }
  else
  {
  	//cout << "CREATE SINGLE Try set last drop" << endl;
    //Add as the last drop
    //Get last drop
    drop* lastDrop = rootDrop;
    while(lastDrop->next != NULL)
	{
    	//cout << "CREATE SINGLE NEXT" << endl;
      lastDrop = lastDrop->next;
    }
    //cout << "CREATE SINGLE Add to list end" << endl;
    //Link last drop to my new drop
    lastDrop->next = d;
    //cout << "CREATE SINGLE Added to list end" << endl;
  }
}

void createDrops(){
  //Prevent running if it is not time yet
  if(millis() < nextDropAttempt) return;

  //cout << "CREATE drop try" << endl;

  //Create drop if chance passes
  if(random(0, 100) > DROP_CHANCE)
  {
  	//cout << "CREATE drop create" << endl;
  	int x = random(0, SCREEN_W);
  	float speed = random(50,150)/100.0;
    createDrop(x, 0, speed);
    //cout << "CREATE ROOT X " << rootDrop->X << " Y " << rootDrop->Y << " drop speed " << rootDrop->Speed << endl;
  }

  //Schedule next drop
  nextDropAttempt = millis() + DROP_INTERVAL;
}

void removeDrop(drop* previous, drop* current)
{
  //cout << "REMOVE current x " << current->X << endl;
  
  if(previous != NULL)
  {
  	//cout << "REMOVE relink try" << endl;
    //Relink previous to next
    previous->next = current->next;
    //cout << "REMOVE relink done" << endl;
  }
  
  if(current == rootDrop)
  {
    rootDrop = rootDrop->next;
  }
  
  //cout << "REMOVE free try" << endl;
  //Delete current
  free(current);
  //cout << "REMOVE free done" << endl;
}

void updateDrops()
{
  //cout << "UPDATE start" << endl;
  drop* currentDrop = rootDrop;
  drop* previousDrop = NULL;
  //cout << "UPDATE prepared" << endl;
  
  while(currentDrop != NULL)
  {
    //Move drop Y
    //cout << "UPDATE drop X " << currentDrop->X << " Y " << currentDrop->Y << " drop speed " << currentDrop->Speed << endl;
    currentDrop->Y += currentDrop->Speed;
    //cout << "UPDATE drop Y " << currentDrop->Y << endl;

    //Check drop Y
    if(currentDrop->Y - DROP_LENGTH > SCREEN_H)
	{
      //Drop ended, remove it
      //cout << "UPDATE remove drop" << currentDrop->Y << endl;
      removeDrop(previousDrop, currentDrop);
    }
    
    //Move to next drop
    previousDrop = currentDrop;
    currentDrop = currentDrop->next;
  }
}

void drawDrops(char screen_buffer[SCREEN_W][SCREEN_H])
{
  //cout << "DRAW started " << endl;
  drop* currentDrop = rootDrop;
  
  while(currentDrop != NULL)
  {
    //Get drop start Y
    int startY = ((int)currentDrop->Y) - DROP_LENGTH;
    if(startY < 0) startY = 0;
    //cout << "DRAW startY " << startY << endl;
    
    int targetY = (int)currentDrop->Y;
    if(targetY >= SCREEN_H) targetY = SCREEN_H-1;
    
    //Draw drop line
    for(int y = startY; y < targetY; y++){
    	screen_buffer[currentDrop->X][y] = 'x';
    	//cout << "DRAW drop : x " << currentDrop->X << " y " << y << " starty " << startY << endl;
	}
    
    //Move to next drop
    currentDrop = currentDrop->next;
  }
}
