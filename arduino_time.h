#include <time.h>
#include <stdio.h>
#include <stdlib.h>

clock_t start; 

void start_time()
{
	srand(time(NULL));
	
	//Get the start time of the app
	start = clock();
}

int random(int lower=0, int top=100){
    int rnd = (rand()%(top-lower)) + lower;
    return rnd;
}

long timediff(clock_t t1, clock_t t2) {
    long elapsed;
    elapsed = ((double)t2 - t1) / CLOCKS_PER_SEC * 1000;
    return elapsed;
}

unsigned long millis()
{ 
    return timediff(start, clock());   
}


