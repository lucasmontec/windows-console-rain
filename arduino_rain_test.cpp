#include "drops.h"
#include <iostream>
#include <conio.h>
#include <windows.h>

#define SIZEY 20
#define SIZEX 50

bool running = true;

using namespace std;

void loop();

char screen_buffer[SIZEX][SIZEY];

void print_screen_buffer();

//A hancle to our console
HANDLE Console;

void hidecursor()
{
   CONSOLE_CURSOR_INFO info;
   info.dwSize = 100;
   info.bVisible = FALSE;
   SetConsoleCursorInfo(Console, &info);
}

void clear_screen_buffer()
{
	for(int y=0;y<SIZEY;y++)
	{
        for(int x=0;x<SIZEX;x++)
		{
           screen_buffer[x][y] = ' ';
        }
    }
}

int main()
{
	//Set our console
    Console = GetStdHandle(STD_OUTPUT_HANDLE);
    
    hidecursor();
    
	//Zero the screen buffer
    clear_screen_buffer();
	
	start_time();
	
	while(1) {
		// ESC fecha o programa
        if (GetAsyncKeyState(0x1b)){
            break;
        }
        
		loop();
	}
	return 0;
}

void print_screen_buffer()
{
	//Print the screen buffer
    for(int y = 0; y < SIZEY; y++){
        for(int x = 0; x < SIZEX; x++){
            cout << screen_buffer[x][y];
        }
        cout << endl;
    }
    
    //Clear the cursor pos
    COORD pos = {0,0};
    SetConsoleCursorPosition(Console, pos);
}

void loop()
{
	clear_screen_buffer();
	
	createDrops();	
	
	updateDrops();

	drawDrops(screen_buffer);
	
	print_screen_buffer();
}
